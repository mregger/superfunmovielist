import { DependencyList, useCallback, useEffect, useState } from 'react';

type UnwrapPromise<T> = T extends Promise<infer U> ? U : T;

export function useAsyncDataCallback<
  T extends (...args: any[]) => Promise<any>,
  TArgs extends Parameters<T>,
  TResult extends UnwrapPromise<ReturnType<T>>
>(requestMethod: T, deps: DependencyList, defaultValue?: TResult) {
  const [data, setData] = useState<TResult | undefined>(
    defaultValue || undefined
  );
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState<Error | null | undefined>();

  let isCancelled = false;

  const executeRequest = useCallback(
    async (...params: TArgs) => {
      setIsLoading(true);

      try {
        const response = await requestMethod(...params);
        if (!isCancelled) {
          setData(response);
          setIsLoading(false);
          setError(null);
        }
      } catch (e) {
        setIsLoading(false);
      }
    },
    [setData, setIsLoading, setError, ...deps]
  );

  useEffect(() => {
    return () => {
      isCancelled = true;
    };
  }, deps);

  return { data, isLoading, error, executeRequest };
}

export function useAsyncData<T>(
  requestMethod: () => Promise<T>,
  deps: DependencyList,
  defaultValue?: T
) {
  const [isLoading, setIsLoading] = useState(true);
  const {
    data,
    error,
    isLoading: callbackIsLoading,
    executeRequest,
  } = useAsyncDataCallback(requestMethod, deps, defaultValue);

  useEffect(() => {
    (async () => {
      setIsLoading(true);
      await executeRequest();
      setIsLoading(false);
    })();
  }, deps);

  return {
    data,
    isLoading: isLoading || callbackIsLoading,
    error,
    executeRequest,
  };
}
