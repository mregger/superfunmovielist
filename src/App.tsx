import { Button } from '@material-ui/core';
import { AppLayout } from 'Layout/AppLayout';
import React from 'react';

const App: React.FC = () => {
  return (
    <>
      <AppLayout> </AppLayout>
    </>
  );
};

export { App };
