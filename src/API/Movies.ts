import request from './Request';

const urlRoot = 'http://api.themoviedb.org/3/movie';

/**
 * Interfaces derived from API response objects, hence the different naming standards
 */

export interface IMovie {
  adult: boolean;
  backdrop_path: string;
  genre_ids: number[];
  id: number;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  release_date: string;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
  runtime?: number;
}

export interface IPaged<T> {
  page: number;
  total_pages: number;
  total_results: number;
  results: T[];
}

export async function getMovieList(): Promise<IPaged<IMovie>> {
  const movieList: IPaged<IMovie> = await request(`${urlRoot}/popular`, {
    method: 'GET',
  });
  return movieList;
}

export async function getMovieDetails(movieId: number): Promise<IMovie> {
  const movieDetails: IMovie = await request(`${urlRoot}/${movieId}`, {
    method: 'GET',
  });
  return movieDetails;
}
