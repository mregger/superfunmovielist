const codeMessage: { [key: number]: string } = {
  200: 'Success',
  201: 'New or modified data successfully.',
  202: 'A request hasentered the background queue（Asynchronous task）.',
  204: 'Data deleted successfully.',
  400: 'There was an error with the request and the server could not respond',
  401: "Unauthorized. Please confirm you're logged in and have permission to do this action",
  403: 'User isauthorized，But access is forbidden.',
  404: 'Resource not found',
  406: 'The requested format is not available.',
  410: "The requested resource is permanently deleted，And won't get it again.",
  422: 'When creating an object，A verification error occurred.',
  500: 'Server eerror, pleaseCheck server.',
  502: 'Gateway error.',
  503: 'service is not available，The server is temporarily overloaded or maintained.',
  504: 'Gateway timeout.',
};

/**
 * Exception handler
 */
async function errorHandler(error: { response: Response; data?: any }) {
  const { response, data } = error;

  if (response && response.status) {
    try {
      await checkError(response);
    } catch {
      throw response;
    }

    const errorText =
      data?.message || codeMessage[response.status] || response.statusText;
    const { status, url } = response;
  }
  throw response;
}

type RequestInitWrapper = Omit<RequestInit, 'body'> & {
  body?: any;
  params?: Record<string, any>;
};

export async function request(
  url: string,
  { body, params, ...customConfig }: RequestInitWrapper = {}
) {
  const urlWithApiKey = `${url}?api_key=${process.env.REACT_APP_API_KEY}`;
  const urlWithParams = params
    ? `${urlWithApiKey}&${new URLSearchParams(params).toString()}`
    : urlWithApiKey;

  try {
    const request = fetch(urlWithParams, {
      ...customConfig,
    });

    const response = await request;

    if (!response.ok) {
      return errorHandler({ response });
    }

    if (response.status === 204) {
      return {};
    } else {
      const data = await response.json();
      return data;
    }
  } catch (e) {
    // TODO figure out if the returns here break things by returning undefined?
  }
}

function checkError(error: { status: number }) {
  const status = error.status;
  if (status === 401 || status === 403) {
    return Promise.reject();
  }
  return Promise.resolve();
}

export default request;
