import { MovieDetails } from 'Pages/MovieDetails';
import { MovieList } from 'Pages/MovieList';
import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

const AppRouter: React.FC = () => {
  return (
    <Router>
      <Route path="/:id" children={<MovieDetails />} />
      <Route exact path="/" children={<MovieList />} />
    </Router>
  );
};
export { AppRouter };
