import { AppBar, Toolbar } from '@material-ui/core';
import React from 'react';

const Footer: React.FC = () => {
  return (
    <footer>
      <Toolbar>Copyright {new Date().getFullYear()}</Toolbar>
    </footer>
  );
};

export { Footer };
