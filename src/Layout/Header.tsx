import { AppBar, Toolbar } from '@material-ui/core';
import React from 'react';
import { useLocation } from 'react-router';

interface IProps {}

const Header: React.FC<IProps> = ({}) => {
  return (
    <header>
      <AppBar>
        <Toolbar>Eduardo's Super Fun Movie List!</Toolbar>
      </AppBar>
    </header>
  );
};

export { Header };
