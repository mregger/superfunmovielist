import { Grid } from '@material-ui/core';
import { getMovieDetails } from 'API/Movies';
import { useAsyncData } from 'Hooks/useAsyncData';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

const imageUrl = 'https://image.tmdb.org/t/p';
const MOBILE_WIDTH = 'w92';
const DESKTOP_WIDTH = 'w185';

const MovieDetails: React.FC = () => {
  let { id } = useParams<{ id?: string }>();

  const getImageWidth = () =>
    window.innerWidth <= 768 ? MOBILE_WIDTH : DESKTOP_WIDTH;

  const { data: movie } = useAsyncData(async () => {
    console.dir(id);
    const parsedId = id && parseInt(id);
    return parsedId && getMovieDetails(parsedId);
  }, []);

  return (
    <div style={{ padding: '5%' }}>
      {movie ? (
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <h1>{movie.title}</h1>
          </Grid>

          <Grid item xs={4}>
            <img src={`${imageUrl}/${getImageWidth()}${movie.poster_path}`} />
          </Grid>
          <Grid item xs={8}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <b>Released:</b> {movie.release_date}
              </Grid>
              <Grid item xs={12}>
                <b>Runtime:</b> {movie.runtime || 0}min
              </Grid>
              <Grid item xs={12}>
                <b>Rating:</b> {movie.vote_average.toPrecision(2)}
              </Grid>
              <Grid item xs={12}>
                <b>Language:</b> {movie.original_language}
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            {movie.overview}
          </Grid>
        </Grid>
      ) : null}
    </div>
  );
};

export { MovieDetails };
