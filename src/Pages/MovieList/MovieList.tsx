import { ImageList } from '@material-ui/core';
import { Grid } from '@material-ui/core';
import { getMovieDetails, getMovieList, IMovie } from 'API/Movies';
import { useAsyncData } from 'Hooks/useAsyncData';
import React from 'react';
import { MovieCard } from './Components/MovieCard';

const MovieList: React.FC = () => {
  const { data: movieList } = useAsyncData(() => getMovieList(), []);

  return (
    <div>
      <ImageList>
        {movieList?.results?.map(
          (movie: IMovie) =>
            movie && (
              <MovieCard
                title={movie.title}
                posterPath={movie.poster_path}
                id={movie.id}
              />
            )
        )}
      </ImageList>
    </div>
  );
};

export { MovieList };
