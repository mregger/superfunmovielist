import { Card, ImageListItem, makeStyles } from '@material-ui/core';
import React from 'react';

const imageUrl = 'https://image.tmdb.org/t/p/w185/';

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));

export interface IMovieCardProps {
  title: string;
  posterPath: string;
  id: number;
}

const MovieCard: React.FC<IMovieCardProps> = ({ title, posterPath, id }) => {
  const classes = useStyles();

  return (
    <ImageListItem key={id}>
      <a href={`/${id}`}>
        <Card title={title}>
          <img src={`${imageUrl}/${posterPath}`}></img>
        </Card>
      </a>
    </ImageListItem>
  );
};

export { MovieCard };
